<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Localization\Localization;
use App\Models\Course;
use App\Models\Translations\CourseTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $courses = Course::with(['translation'])->get()->filter(function($translations){
            if($translations->translation != null){
                return $translations;
            }
        });
        return view('admin.course.index',['courses' => $courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name.*'            => 'required|max:255',
            'about.*'           => 'required',
            'description.*'     => 'required',
            'requirement.*'     => 'required',
            'price'             => 'required|numeric',
            'image'             => 'required|mimes:jpeg,png,jpg|max:2048',
            'level_id'          => 'required|exists:levels,id',
            'payment_id'        => 'required|exists:payments,id',
            'duration'          => 'required|integer|between:1,5'
        ]);


        $course = Course::create([
            'price'         => $request->price,
            'image'         => Storage::putFile('public/course',$request->file('image')),
            'duration_id'   => $request->duration,
            'level_id'      => $request->level_id,
            'payment_id'    => $request->payment_id
        ]);

        $data = [];

        foreach ($request->name as $locale => $name){
            $data[] = [
                'name'          => $name,
                'about'         => $request->about[$locale],
                'description'   => $request->description[$locale],
                'requirement'   => $request->requirement[$locale],
                'locale'        => $locale,
                'slug'          => Str::slug($name)
            ];
        }

        $course->translations()->createMany($data);
        return redirect()->route('admin.course.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        return view('admin.course.edit',['course' => $course]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name.*'            => 'required|max:255',
            'about.*'           => 'required',
            'description.*'     => 'required',
            'requirement.*'     => 'required',
            'price'             => 'required|numeric',
            'image'             => 'mimes:jpeg,png,jpg|max:2048',
            'level_id'          => 'required|exists:levels,id',
            'payment_id'        => 'required|exists:payments,id',
            'duration'          => 'required|integer|between:1,5'
        ]);


        $course = Course::findOrFail($id);

        $course->price      = $request->price;
        $course->level_id   = $request->level_id;

        $course->payment_id = $request->payment_id;
        $course->duration_id= $request->duration;

        if($request->hasFile('image')){
            Storage::delete($course->image);
            $course->image  = Storage::putFile('public/course',$request->file('image'));
        }
        $course->save();


        $data = [];
        foreach ($request->name as $locale => $name){
            $data[] = [
                'name'          => $name,
                'about'         => $request->about[$locale],
                'description'   => $request->description[$locale],
                'requirement'   => $request->requirement[$locale],
                'locale'        => $locale,
                'slug'          => Str::slug($name)
            ];
        }

        $course->translations()->delete();
        $course->translations()->createMany($data);
        return redirect()->route('admin.course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Course::destroy($id);
        return response()->json(['success' => true]);
    }
}
