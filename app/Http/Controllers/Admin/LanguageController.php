<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        return view('admin.language.index',['languages' => Language::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'locale' => 'required|max:4',
            'active' => 'required|between:0,1',
            'image'  => 'required|mimes:png'
        ]);

        $fileName = $request->locale.'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move('assets/admin/lang/',$fileName);

        Language::create([
            'name'      => $request->name,
            'locale'    => $request->locale,
            'active'    => $request->active,
            'image'     => $fileName
        ]);
        return redirect()->route('admin.language.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $language = Language::findOrFail($id);
        return  view('admin.language.edit',['language' => $language]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'locale' => 'required|max:4',
            'active' => 'required|between:0,1',
            'image'  => 'mimes:png'
        ]);

        $language = Language::findOrFail($id);

        if($request->hasFile('image')){
            $fileName = $request->locale.'.'.$request->file('image')->getClientOriginalExtension();
            File::delete('assets/admin/lang/'.$language->image);
            $request->file('image')->move('assets/admin/lang/',$fileName);
            $language->image = $fileName;
        }

        $language->locale = $request->locale;
        $language->name = $request->name;
        $language->active = $request->active;
        $language->save();

        return redirect()->route('admin.language.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $language = Language::findOrFail($id);
        File::delete('assets/admin/lang/'.$language->image);
        Language::destroy($id);
        return response()->json(['success' => true]);
    }
}
