<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $levels = Level::all();
        return view('admin.level.index',['levels' => $levels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name.*'   => 'required'
        ]);

        $level = Level::create([]);
        $data = [];

        foreach ($request->name as $locale => $name){
            $data[] = [
                'name' => $name,
                'locale' => $locale
            ];
        }

        $level->translations()->createMany($data);
        return redirect()->route('admin.level.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $level = Level::findOrFail($id);
        return view('admin.level.edit',['level' => $level]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name.*'  => 'required',
        ]);

        $level = Level::findOrFail($id);
        $data  = [];

        foreach ($request->name as $locale => $name){
            $data[] = [
                'name'   => $name,
                'locale' => $locale
            ];
        }

        $level->translations()->delete();
        $level->translations()->createMany($data);

        return redirect()->route('admin.level.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Level::destroy($id);
        return response()->json(['success' => true]);
    }
}
