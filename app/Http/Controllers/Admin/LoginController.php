<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(){

        if(Auth::guard('admin')->check()){
            return redirect()->route('admin.dashboard');
        }

        return view('admin.login');
    }

    public function auth(AdminLoginRequest $request){
        if(Auth::guard('admin')->attempt($request->validated())){
            return redirect()->route('admin.dashboard');
        }else{
            return back()->with('error','Email or Password invalid');
        }
    }

    public function dashboard(){
        return view('admin.dashboard');
    }

    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

}
