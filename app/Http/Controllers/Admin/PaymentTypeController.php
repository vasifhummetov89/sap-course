<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $payments = Payment::all();

        return view('admin.payment.index',['payments' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.payment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name.*'   => 'required'
        ]);

        $payment = Payment::create([]);
        $data = [];

        foreach ($request->name as $locale => $name){
            $data[] = [
                'name' => $name,
                'locale' => $locale
            ];
        }

        $payment->translations()->createMany($data);
        return redirect()->route('admin.payment-type.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $payment = Payment::findOrFail($id);
        return view('admin.payment.edit',['payment' => $payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name.*'   => 'required'
        ]);

        $payment = Payment::findOrFail($id);
        $data = [];

        foreach ($request->name as $locale => $name){
            $data[] = [
                'name' => $name,
                'locale' => $locale
            ];
        }

        $payment->translations()->delete();
        $payment->translations()->createMany($data);

        return redirect()->route('admin.payment-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
       Payment::destroy($id);
       return response()->json(['success' => true]);
    }
}
