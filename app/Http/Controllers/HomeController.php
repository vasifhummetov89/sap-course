<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Course;
use App\Models\Level;
use App\Models\Payment;
use App\Models\University;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index(){
        $courses        = Course::with(['translation'])->limit(8)->orderBy('id','DESC')->get();
        $companies      = Company::orderByDesc('id')->get();
        $universities   = University::orderByDesc('id')->get();
        return view('index',[
            'courses' => $courses,
            'companies' => $companies,
            'universities' => $universities,
        ]);
    }

    public function companies(){
        $companies = Company::orderByDesc('id')->get();
        return view('companies',[
            'companies' => $companies
        ]);
    }

    public function courses(){
        $courses    = Course::orderBy('id','DESC')->paginate(6);
        $levels     = Level::all();
        $payments   = Payment::all();
        return view('courses',[
            'courses'   => $courses,
            'levels'    => $levels,
            'payments'  => $payments
        ]);
    }

    public function course($slug = null){
        return view('course');
    }

    public function about(){
        return view('about');
    }

    public function jobs(){
        return view('jobs');
    }

    public function contact(){
        return view('contact');
    }

    public function register(){
        return view('register');
    }

    public function registerStore(Request $request){

        $request->validate([
            'name'      => 'required|max:255',
            'surname'   => 'required|max:255',
            'phone'     => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|confirmed|max:50'
        ]);


        $user = User::create([
            'name'          => $request->name,
            'surname'       => $request->surname,
            'email'         => $request->email,
            'password'      => bcrypt($request->password)
        ]);

        $user->phone()->create(['phone' => $request->phone]);

        return redirect()->route('index')->with(['success' => true,'title' => 'Təşəkkür edirik','message' => 'Ugurla qeyd oldunuz','icon' => 'success']);

    }

    public function login(){
        return view('login');
    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::guard('speaker')->logout();
        Auth::guard('web')->logout();
        return redirect()->route('index');
    }

    public function signIn(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|max:50'
        ]);

        if(Auth::guard('web')->attempt([
            'email'   => $request->email,
            'password' => $request->password,
        ])){
            return redirect()->route('index');
        }elseif(Auth::guard('speaker')->attempt([
            'email'   => $request->email,
            'password' => $request->password,
        ])){
            return redirect()->route('index');
        }else{
            return redirect()->route('index')
                ->with([
                    'success' => true,
                    'title' => 'Təhlükə',
                    'message' => 'Email və ya şifrə yalnışdır',
                    'icon' => 'warning'
                ]);
        }

    }

    public function teacher_settings(){
        $user = Auth::guard('speaker')->user();
    }


    public function student_settings(){

        $user = Auth::guard('web')->user();
//        dd($user->course()->first());

        return view('student');
    }

    public function studentEditProfile(){

    }

}
