<?php


namespace App\Http\Localization;


use App\Models\Language;
use Illuminate\Support\Facades\App;

class Localization
{

    public static function getLocales(){
        return Language::where('active',1)->get();
    }

    public static function getCurrentLocale(){
        $languages = Language::where('active',1)->get();
        $contains = $languages->contains('locale',request()->segment(1));
        if($contains){
            App::setLocale(request()->segment(1));
            return request()->segment(1);
        }else{
            App::setLocale(config('app.locale'));
            return '/';
        }
    }

}
