<?php

namespace App\Models;

use App\Models\Translations\CourseTranslation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Course extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function translations(){
        return $this->hasMany(CourseTranslation::class)
            ->rightJoin('languages',function ($join){
                $join->on('languages.locale','course_translations.locale');
            })
            ->orWhereNull('course_translations.course_id')
            ->where('languages.active',1)->select('course_translations.*','languages.locale');
    }


    public function translation(){
        return $this->hasOne(CourseTranslation::class)->where('locale',App::getLocale());
    }

    public function level(){
        return $this->belongsTo(Level::class);
    }

    public function payment(){
        return $this->belongsTo(Payment::class);
    }

}
