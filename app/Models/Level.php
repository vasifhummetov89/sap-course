<?php

namespace App\Models;

use App\Models\Translations\LevelTranslation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Level extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];

    public function translation(){
        return $this->hasOne(LevelTranslation::class)->where('locale',App::getLocale());
    }

    public function translations(){
        return $this->hasMany(LevelTranslation::class)->rightJoin('languages',function ($join){
            $join->on('languages.locale','level_translations.locale');
        })
            ->orWhereNull('level_translations.course_id')
            ->where('languages.active',1)->select('level_translations.*','languages.locale');
    }
}
