<?php

namespace App\Models;

use App\Models\Translations\PaymentTranslation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Payment extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];

    public function translation(){
        return $this->hasOne(PaymentTranslation::class)->where('locale',App::getLocale());
    }

    public function translations(){
        return $this->hasMany(PaymentTranslation::class)->rightJoin('languages',function ($join){
            $join->on('languages.locale','payment_translations.locale');
        })
            ->orWhereNull('payment_translations.course_id')
            ->where('languages.active',1)->select('payment_translations.*','languages.locale');
    }
}
