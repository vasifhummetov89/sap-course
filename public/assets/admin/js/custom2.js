$('.widget-content .warning.confirm').on('click', function () {

    action   = $(this).data('action');
    redirect = $(this).data('redirect');

    swal({
        title: 'Are you sure?',
        text: "This data is deleted? ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        padding: '2em'
    }).then(function(result) {

        if (result.value) {
            $.ajax({
                url: action,
                method: 'POST',
                data: {_method: 'DELETE'},
                success: function(response){
                    if(response.success === true){
                        swal(
                            'Deleted!',
                            'Your data has been deleted.',
                            'success'
                        );
                        window.location.href = redirect;
                    }
                }
            });
        }
    })
});