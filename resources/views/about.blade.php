@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/details.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')

    <section class="course-banner-section" >
        <div class="auto-container">
            <h1>About Us</h1>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?" required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--Education Section-->

    <section class="education-section section-second">
        <div class="patern-layer-one" style="background-image: url('/assets/images/icons/icon-1.png');"></div>
        <div class="patern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="auto-container">
            <div class="row">
                <div class="image-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <img src="/assets/images/courses/c.png" alt="">
                    </div>
                </div>
                <div class="text-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <h2>About Us</h2>
                        <div class="text">
                            <p>
                                Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two waters own morning gathered greater shall had behold had seed.
                            </p>
                        </div>
                        <a href="course.html" class="theme-btn btn-course"><span class="txt">Learn More <i class="fa fa-angle-right"></i></span></a>
                        <a href="course.html" class="theme-btn btn-start"><span class="txt">Check Certification <i class="fa fa-angle-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Counted Section-->
    <section class="achievements-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2>Our achievements</h2>
                <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re <br> two waters own morning gathered greater shall had behold had seed.</div>
            </div>

            <!-- Fact Counter -->
            <div class="fact-counter">
                <div class="row clearfix">

                    <!-- Column -->
                    <div class="column counter-column col-lg-4 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="icon-box">
                                    <img src="/assets/images/icons/reading.svg" alt="">
                                </div>
                                <h4 class="counter-title">Total Students</h4>
                                <div class="count-outer">
                                    <span class="count-number" >34</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Column -->
                    <div class="column counter-column col-lg-4 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="icon-box">
                                    <img src="/assets/images/icons/member.svg" alt="">
                                </div>
                                <h4 class="counter-title">Attendend Students</h4>
                                <div class="count-outer count-box ">
                                    <span class="count-number"  >17</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Column -->
                    <div class="column counter-column col-lg-4 col-md-6 col-sm-12">
                        <div class="inner">
                            <div class="content">
                                <div class="icon-box">
                                    <img src="/assets/images/icons/guarantee.svg" alt="">
                                </div>
                                <h4 class="counter-title">Graduated Students</h4>
                                <div class="count-outer ">
                                    <span class="count-number" >22</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>


    <!--Teachers Section-->
    <section class="teachers-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2>Our Speakers</h2>
            </div>

            <!-- Fact Counter -->
            <div class="fact-counter">
                <div class="row clearfix">

                    <!-- Column -->
                    <div class="col-lg-3 col-md-3 col-sm-6 course-col">
                        <div class="inner-box">
                            <div class="image">
                                <a href="#">
                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="inner-content">
                                <div class="title-part">
                                    <div class="left-side">
                                        <a href="#" class="detail-link">
                                            Anna Maria
                                        </a>
                                    </div>
                                </div>
                                <div class="footer-part">
                                    <div class="text">SAP Teacher</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Column -->
                    <div class="col-lg-3 col-md-3 col-sm-6 course-col">
                        <div class="inner-box">
                            <div class="image">
                                <a href="#">
                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="inner-content">
                                <div class="title-part">
                                    <div class="left-side">
                                        <a href="#" class="detail-link">
                                            Anna Maria
                                        </a>
                                    </div>
                                </div>
                                <div class="footer-part">
                                    <div class="text">SAP Teacher</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Column -->
                    <div class="col-lg-3 col-md-3 col-sm-6 course-col">
                        <div class="inner-box">
                            <div class="image">
                                <a href="#">
                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="inner-content">
                                <div class="title-part">
                                    <div class="left-side">
                                        <a href="#" class="detail-link">
                                            Anna Maria
                                        </a>
                                    </div>
                                </div>
                                <div class="footer-part">
                                    <div class="text">SAP Teacher</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Column -->
                    <div class="col-lg-3 col-md-3 col-sm-6 course-col">
                        <div class="inner-box">
                            <div class="image">
                                <a href="#">
                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                </a>
                            </div>
                            <div class="inner-content">
                                <div class="title-part">
                                    <div class="left-side">
                                        <a href="#" class="detail-link">
                                            Anna Maria
                                        </a>
                                    </div>
                                </div>
                                <div class="footer-part">
                                    <div class="text">SAP Teacher</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <!--Option Section-->

    <section class="options-section" style="background-image: url('/assets/images/banners/3.png');">
        <div class="auto-container">
            <div class="content">
                <h2>Do you want to join us?</h2>
            </div>
            <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two <br> waters own morning gathered greater shall had behold had seed.</div>
            <div class="buttons-box">
                <a href="instuctor.html" class="theme-btn btn-start"><span class="txt">Become Instuctor <i class="fa fa-angle-right"></i></span></a>
                <a href="student.html" class="theme-btn btn-course"><span class="txt">Start Learning  <i class="fa fa-angle-right"></i></span></a>
            </div>
        </div>
    </section>

@endsection
