@extends('layouts.admin')
@section('title','Company Create')

@section('css-app')

    <link href="{{asset('assets/admin/css/loader.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('assets/admin/js/loader.js')}}"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/admin/css/plugins.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/table/datatable/custom_dt_html5.css">
    <link rel="stylesheet" type="text/css" href="/assets/admin/plugins/table/datatable/dt-global_style.css">

    <link href="/assets/admin/plugins/animate/animate.css" rel="stylesheet" type="text/css" />
    <script src="/assets/admin/plugins/sweetalerts/promise-polyfill.js"></script>
    <link href="/assets/admin/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />

    <!--File Uploads-->
    <link href="/assets/admin/plugins/file-upload/file-upload-with-preview.min.css" rel="stylesheet" type="text/css" />

@endsection



@section('content')

    <div id="content" class="main-content">
        <div class="container">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row layout-top-spacing">
                    <div id="fuMultipleFile" class="col-lg-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>Multiple File Upload</h4>
                                    </div>
                                </div>
                            </div>
                            <form action="{{route('admin.company.store')}}" method="POST" enctype="multipart/form-data">
                                <div class="widget-content widget-content-area">
                                    <div class="custom-file-container" data-upload-id="mySecondImage">
                                        <label>Upload (Allow Multiple) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                        <label class="custom-file-container__custom-file" >
                                            @csrf
                                            <input type="file" class="custom-file-container__custom-file__custom-file-input" name="files[]" multiple>
                                            <span class="custom-file-container__custom-file__custom-file-control"></span>
                                        </label>
                                        <div class="custom-file-container__image-preview"></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Upload</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com">DesignReset</a>, All rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="/assets/admin/js/libs/jquery-3.1.1.min.js"></script>
    <script src="/assets/admin/bootstrap/js/popper.min.js"></script>
    <script src="/assets/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/assets/admin/js/app.js"></script>

    <script>
        $(document).ready(function () {
            App.init();
        });
    </script>
    <script src="/assets/admin/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="/assets/admin/plugins/table/datatable/datatables.js"></script>

    <script src="/assets/admin/plugins/table/datatable/button-ext/dataTables.buttons.min.js"></script>
    <script src="/assets/admin/plugins/table/datatable/button-ext/jszip.min.js"></script>
    <script src="/assets/admin/plugins/table/datatable/button-ext/buttons.html5.min.js"></script>
    <script src="/assets/admin/plugins/table/datatable/button-ext/buttons.print.min.js"></script>

    <script>
        $('#html5-extension').DataTable({
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    {extend: 'copy', className: 'btn'},
                    {extend: 'csv', className: 'btn'},
                    {extend: 'excel', className: 'btn'},
                    {extend: 'print', className: 'btn'}
                ]
            },
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>

    <script src="/assets/admin/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="/assets/admin/plugins/sweetalerts/custom-sweetalert.js"></script>

    <script src="/assets/admin/plugins/file-upload/file-upload-with-preview.min.js"></script>
    <script>
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>

@endsection
