@extends('layouts.admin')

@section('title','Course Create')

@section('css-app')

    <link href="{{asset('assets/admin/css/loader.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('assets/admin/js/loader.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="/assets/admin/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    {{--<link rel="stylesheet" type="text/css" href="/assets/admin/plugins/bootstrap-select/bootstrap-select.min.css">--}}
    <link href="/assets/admin/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/plugins/file-upload/file-upload-with-preview.min.css" rel="stylesheet" type="text/css" />


@endsection


@section('content')

    <div id="content" class="main-content">
        <div class="container m-auto">
            <div class="container">

                <div class="col-lg-12 col-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-header">
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>Create Course</h4>
                                </div>
                            </div>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="widget-content widget-content-area border-tab">

                            <ul class="nav nav-tabs mt-3" id="border-tabs" role="tablist">
                                @foreach($languages as $language)
                                    <li class="nav-item">
                                        <a class="nav-link {{$loop->index == 0 ? 'active' : ''}}" id="border-home-tab" data-toggle="tab" href="#lang-{{$language->locale}}" role="tab" aria-controls="border-home" aria-selected="true"><img width="30px;" src="/assets/admin/lang/{{$language->locale}}.png"> {{strtoupper($language->code)}}</a>
                                    </li>
                                @endforeach

                            </ul>
                            <form action="{{route('admin.course.update',['course' => $course->id])}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="tab-content mb-4" id="border-tabsContent">

                                    @foreach($course->translations as $translation)

                                        <div class="tab-pane fade {{$loop->index == 0 ? 'show active' : ''}}" id="lang-{{$translation->locale}}" role="tabpanel" aria-labelledby="border-home-tab">

                                            <label for="course-name">Name ({{$translation->locale}})</label>
                                            <input title="" type="text" value="{{$translation->name}}" class="form-control" id="course-name" name="name[{{$translation->locale}}]" required>

                                            <label for="about">About ({{$translation->locale}})</label>
                                            <textarea class="form-control" name="about[{{$translation->locale}}]" id="about" cols="30" rows="10">{{$translation->about}}</textarea>

                                            <label for="description">Description ({{$translation->locale}})</label>
                                            <textarea class="form-control" name="description[{{$translation->locale}}]" id="description" cols="30" rows="10">{{$translation->description}}</textarea>

                                            <label for="requirement">Requirement ({{$translation->locale}})</label>
                                            <textarea class="form-control" name="requirement[{{$translation->locale}}]" id="requirement" cols="30" rows="10">{{$translation->requirement}}</textarea>
                                        </div>

                                    @endforeach

                                    <div class="form-group mt-9 p-0">
                                        <label for="price">Price</label>
                                        <input type="text" id="price" name="price" value="{{$course->price}}" class="form-control" required>
                                    </div>

                                    <div class="form-group mt-9">
                                        <label for="level">Level</label>
                                        <select name="level_id" id="level" class="form-control">
                                            <option value=""></option>
                                            @foreach(\App\Models\Level::all() as $level)
                                                <option value="{{$level->id}}" @if($level->id == $course->level_id) selected @endif>{{$level->translation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group mt-9">
                                        <label for="payment_id">Payment Type</label>
                                        <select name="payment_id" id="payment_id" class="form-control">
                                            <option value=""></option>
                                            @foreach(\App\Models\Payment::all() as $payment)
                                                <option value="{{$payment->id}}" @if($payment->id == $course->payment_id) selected @endif>{{$payment->translation->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group mt-9">
                                        <label for="duration">Duration</label>
                                        <select name="duration" id="duration" class="form-control">
                                            <option value=""></option>
                                            @foreach(range(1,5) as $duration)
                                                <option value="{{$duration}}" @if($duration == $course->duration_id) selected @endif> {{$duration}} Month</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="widget-content widget-content-area">
                                        <div class="custom-file-container" data-upload-id="myFirstImage">
                                            <label>Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                            <label class="custom-file-container__custom-file" >
                                                <input type="file" class="custom-file-container__custom-file__custom-file-input" name="image" accept="image/*">
                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                            </label>
                                            <div class="custom-file-container__image-preview"></div>
                                        </div>
                                    </div>
                                    <button type="submit" class="mt-4 btn btn-primary float-right">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection


@section('js')

    <script src="/assets/admin/js/libs/jquery-3.1.1.min.js"></script>
    <script src="/assets/admin/bootstrap/js/popper.min.js"></script>
    <script src="/assets/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/assets/admin/js/app.js"></script>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="/assets/admin/plugins/highlight/highlight.pack.js"></script>
    <script src="/assets/admin/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="/assets/admin/js/scrollspyNav.js"></script>
    {{-- <script src="/assets/admin/plugins/bootstrap-select/bootstrap-select.min.js"></script>--}}
    <script src="/assets/admin/plugins/file-upload/file-upload-with-preview.min.js"></script>

    <script>
        var firstUpload = new FileUploadWithPreview('myFirstImage');
    </script>

@endsection


