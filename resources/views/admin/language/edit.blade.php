@extends('layouts.admin')

@section('title','Create Language')

@section('css-app')

    <link href="{{asset('assets/admin/css/loader.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('assets/admin/js/loader.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="/assets/admin/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    {{--<link rel="stylesheet" type="text/css" href="/assets/admin/plugins/bootstrap-select/bootstrap-select.min.css">--}}
    <link href="/assets/admin/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
    <link href="/assets/admin/plugins/file-upload/file-upload-with-preview.min.css" rel="stylesheet" type="text/css" />


@endsection


@section('content')

    <div id="content" class="main-content">
        <div class="container m-auto">
            <div class="container">

                <div class="col-lg-12 col-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-header">
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>Create Language</h4>
                                </div>
                            </div>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="widget-content widget-content-area border-tab">
                            <form action="{{route('admin.language.update',[$language->id])}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="tab-content mb-4" id="border-tabsContent">
                                    <div class="tab-pane fade show active" id="lang-1" role="tabpanel" aria-labelledby="border-home-tab">
                                        <label for="name">Name </label>
                                        <input title="" type="text" value="{{$language->name}}" class="form-control" id="name" name="name" required>
                                        <label for="locale">Locale </label>
                                        <input title="" type="text" value="{{$language->locale}}" class="form-control" id="locale" name="locale" required>

                                        <label for="active">Active</label>
                                        <select name="active" id="active" class="form-control">
                                            <option value="1" @if($language->active == 1) selected @endif>Active</option>
                                            <option value="0" @if($language->active == 0) selected @endif>Deactive</option>
                                        </select>

                                        <div class="widget-content widget-content-area">
                                            <div class="custom-file-container" data-upload-id="myFirstImage">
                                                <label>Flag Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                                                <label class="custom-file-container__custom-file" >
                                                    <input type="file" class="custom-file-container__custom-file__custom-file-input" name="image" accept="image/*">
                                                    <span class="custom-file-container__custom-file__custom-file-control"></span>
                                                </label>
                                                <div class="custom-file-container__image-preview"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="mt-4 btn btn-primary float-right">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')

    <script src="/assets/admin/js/libs/jquery-3.1.1.min.js"></script>
    <script src="/assets/admin/bootstrap/js/popper.min.js"></script>
    <script src="/assets/admin/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/admin/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/assets/admin/js/app.js"></script>

    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="/assets/admin/plugins/highlight/highlight.pack.js"></script>
    <script src="/assets/admin/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="/assets/admin/js/scrollspyNav.js"></script>
    {{-- <script src="/assets/admin/plugins/bootstrap-select/bootstrap-select.min.js"></script>--}}
    <script src="/assets/admin/plugins/file-upload/file-upload-with-preview.min.js"></script>

    <script>
        var firstUpload = new FileUploadWithPreview('myFirstImage');
    </script>

@endsection



