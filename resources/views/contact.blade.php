@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="/assets/build/css/intlTelInput.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

@endsection

@section('content')

    <section class="contact-section">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="contact-content">
            <div class="auto-container">
                <div class="row">
                    <div class="contact-body">
                        <div class="col-12">
                            <form id="form-sign-up" action="" autocomplete="off">
                                <div class="title-box col-12">
                                    <h2 >Bizimlə Əlaqə Saxlayın</h2>
                                    <div class="text">İstək və təkliflərinizi bizə göndərin</div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="username" value="" placeholder=" Name" id="username" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" name="usersurname" value="" placeholder=" Surname" id="username" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <input type="password" id="email" name="password" value="" placeholder="Email" required="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <input type="number" class="form-control"  placeholder="Telephone" id="tel">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <textarea class="" name="message" placeholder="Send Message"></textarea>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group text-center">
                                            <button  class="theme-btn btn-style-three"><span class="txt">Send message</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-info-section">
            <div class="title-box">
                <h2>Contact Information</h2>
                <div class="text">Lorem Ipsum is simply dummy text of the printing <br> and typesetting industry.</div>
            </div>

            <div class="row clearfix">

                <!-- Info Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="info-inner">
                        <div class="icon fa fa-phone-alt"></div>
                        <strong>Phone</strong>
                        <ul>
                            <li><a href="tel:+1-123-456-7890">+1 (123) 456-7890</a></li>
                            <li><a href="tel:+1-123-456-7890">+1 (123) 456-7890</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Info Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="info-inner">
                        <div class="icon far fa-envelope"></i></div>
                        <strong>Email</strong>
                        <ul>
                            <li><a href="mailto:info@yourcompany.com">info@yourcompany.com</a></li>
                            <li><a href="mailto:infobootcamp@gmail.com">infobootcamp@gmail.com</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Info Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="info-inner">
                        <div class="icon fa fa-map-marker-alt"></div>
                        <strong>Address</strong>
                        <ul>
                            <li>Portfolio Technology 07, Capetown 12 Road, Chicago, 2436, USA</li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

@section('js')

    <script src="/assets/build/js/intlTelInput-jquery.min.js"></script>
    <script>
        $("#tel").intlTelInput({// whether or not to allow the dropdown
            allowDropdown: true,

            // if there is just a dial code in the input: remove it on blur, and re-add it on focus
            autoHideDialCode: true,

            // add a placeholder in the input with an example number for the selected country
            autoPlaceholder: "polite",

            // modify the auto placeholder
            customPlaceholder: null,

            // append menu to specified element
            dropdownContainer: null,

            // don't display these countries
            excludeCountries: [],

            // format the input value during initialisation and on setNumber
            formatOnDisplay: true,

            // geoIp lookup function
            geoIpLookup: null,

            // inject a hidden input with this name, and on submit, populate it with the result of getNumber
            hiddenInput: "",

            // initial country
            initialCountry: "",

            // localized country names e.g. { 'de': 'Deutschland' }
            localizedCountries: null,

            // don't insert international dial codes
            nationalMode: true,

            // display only these countries
            onlyCountries: [],

            // number type to use for placeholders
            placeholderNumberType: "MOBILE",

            // the countries at the top of the list. defaults to united states and united kingdom
            preferredCountries: [ "us", "gb" ],

            // display the country dial code next to the selected flag so it's not part of the typed number
            separateDialCode: false,

            // specify the path to the libphonenumber script to enable validation/formatting
            utilsScript: ""
        });

    </script>

@endsection
