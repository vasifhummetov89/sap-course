@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/details.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')


    <section class="course-banner-section" >
        <div class="auto-container">
            <h1>Courses</h1>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?" required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="course-list">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="circle-one"></div>
        <div class="auto-container">
            <div class="row">
                <div class="course-header">
                    <div class="detail-title">
                        <h3>
                            Learn User Interface and User Experience
                        </h3>
                    </div>
                </div>
                <div class="content-side col-lg-9 col-md-12 col-sm-12">
                    <div class="inner-column tab-col">
                        <div class="info-tabs">
                            <div class="info-tabs-inner tabs-box">
                                <ul class="tab-btns tab-buttons">
                                    <!-- <li data-tab ="overview" class="tab-btn active-btn" data-title="overview">Overview</li> -->
                                    <!-- <li data-tab ="faq" class="tab-btn" data-title="faq">FAQ</li> -->
                                </ul>

                                <div class="tabs-content">
                                    <div class="tab active-tab" id="overview">
                                        <div class="">
                                            <div class="course-overview">
                                                <div class="inner-box-course">
                                                    <h4>About Our SAP Course</h4>
                                                    <p>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur.</p>
                                                    <ul class="review-list">
                                                        <li>23,564 Total Students</li>
                                                        <li>
                                                            <span class="theme_color">4.5</span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            <span class="fa fa-star"></span>
                                                            (1254 Rating)
                                                        </li>
                                                    </ul>
                                                    <h3>
                                                        What you will learn?
                                                    </h3>
                                                    <ul class="review-list">
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                        <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        <li>Sed consequat justo non mauris pretium at tempor justo.</li>
                                                        <li>Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                    </ul>
                                                    <h3>
                                                        Requirements
                                                    </h3>
                                                    <ul class="requirement-list">
                                                        <li>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo</li>
                                                        <li>Ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel.</li>
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                        <li>Varius et commodo ut, ultricies vitae velit. Ut nulla tellus.</li>
                                                        <li>Phasellus enim magna, varius et commodo ut.</li>
                                                    </ul>
                                                    <h3>
                                                        Instructors
                                                    </h3>
                                                    <div class="row">
                                                        <div class="teacher-block col-lg-6 col-md-6 col-sm-12">
                                                            <div class="block-inner">
                                                                <div class="image">
                                                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                                                </div>
                                                                <h2>Lala Huseynova</h2>
                                                                <div class="text">Certified instructor Architecture& Developer</div>
                                                                <div class="social-box">
                                                                    <a href="#" class="fa fa-facebook-square"></a>
                                                                    <a href="#" class="fa fa-linkedin-square"></a>
                                                                    <a href="#" class="fa fa-github"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="teacher-block col-lg-6 col-md-6 col-sm-12">
                                                            <div class="block-inner">
                                                                <div class="image">
                                                                    <img src="/assets/images/courses/student-2.jpg" alt="">
                                                                </div>
                                                                <h2>Araz Mammadov</h2>
                                                                <div class="text">Certified instructor Architecture& Developer</div>
                                                                <div class="social-box">
                                                                    <a href="#" class="fa fa-facebook-square"></a>
                                                                    <a href="#" class="fa fa-linkedin-square"></a>
                                                                    <a href="#" class="fa fa-github"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-side col-lg-3 col-md-12 col-sm-12">
                    <div class="inner-column-side ">
                        <div class="price">$11.99</div>
                        <div class="time-left">23 hours left at this price!</div>
                        <a href="#" class="theme-btn btn-style-three"><span class="txt">Apply Now <i class="fa fa-angle-right"></i></span></a>
                    </div>
                    <div class="inner mt-20">
                        <div class="content">
                            <div class="iconbox">
                                <div class="icon">
                                    <img src="/assets/images/icons/process.svg" alt="">
                                </div>
                            </div>
                            <h4 class="course-title">Business Management Academy</h4>
                            <a href="" class="btn-pink">
                                Read more
                            </a>
                        </div>
                    </div>
                    <div class="inner mt-20">
                        <div class="content">
                            <div class="iconbox">
                                <div class="icon">
                                    <img src="/assets/images/icons/technical-support.svg" alt="">
                                </div>
                            </div>
                            <h4 class="course-title">Information Technology Academy</h4>
                            <a href="" class="btn-pink">
                                Read more
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="options-section" style="background-image: url('/assets/images/banners/3.png');">
        <div class="auto-container">
            <div class="content">
                <h2>Do you want to join us?</h2>
            </div>
            <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two <br> waters own morning gathered greater shall had behold had seed.</div>
            <div class="buttons-box">
                <a href="instuctor.html" class="theme-btn btn-start"><span class="txt">Become Instuctor <i class="fa fa-angle-right"></i></span></a>
                <a href="student.html" class="theme-btn btn-course"><span class="txt">Start Learning  <i class="fa fa-angle-right"></i></span></a>
            </div>
        </div>
    </section>

@endsection
