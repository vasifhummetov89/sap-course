@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection


@section('content')

    <section class="course-banner-section" >
        <div class="auto-container">
            <h1>Courses</h1>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?" required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--Course List Section-->

    <div class="course-list">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>

        <div class="auto-container">
            <div class="row">
                <div class="content-side col-lg-9 col-md-12 col-sm-12">
                    <div class="our-courses">
                        <div class="course-header">
                            <div class="course-title">
                                <h3>Browse Our Courses</h3>
                            </div>
                            <div class="course-view ">
                                <div class="list-view-button "><i class="fa fa-bars active" aria-hidden="true"></i></div>
                                <div class="grid-view-button"><i class="fa fa-th-large" aria-hidden="true"></i></div>
                            </div>
                            <div>

                            </div>
                        </div>
                        <div class="inner-part">
                            <ol class="list list-view-filter">
                                @forelse($courses as $course)
                                    <li class="inner-list">
                                        <a href="{{route('course',['slug' => $course->translation->slug])}}" class="inner-element">
                                            <img src="{{\Illuminate\Support\Facades\Storage::url($course->image)}}" alt="">
                                        </a>
                                        <div class="inside-content">
                                            <h5><a href="{{route('course',['slug' => $course->translation->slug])}}">{{$course->translation->name}}</a></h5>
                                            <div class="text mb-0">
                                                <p>
                                                    {{\Illuminate\Support\Str::limit($course->translation->about)}}
                                                </p>
                                            </div>
                                            <div class="inner-detail">
                                                <div class="inner-count">
                                                    <span>125 Students</span>
                                                </div>
                                                <div class="inner-hour">
                                                    <span>{{$course->duration_id}} Month</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @empty
                                    Empty
                                @endforelse
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="sidebar-side col-lg-3 col-md-12 col-sm-12">
                    <div class="sidebar-inner">
                        <aside class="sidebar">
                            <div class="filter-box">
                                <h5>Filter By</h5>
                                <div class="skills-box">
                                    <div class="skill-form">
                                        <form action="" method="post">
                                            <span>Skill Level</span>
                                            @foreach($levels as $level)
                                                <div class="radio-box">
                                                    <label class="p-0" for="level-{{$loop->index}}">
                                                        <input type="checkbox" name="level" id="level-{{$loop->index}}">
                                                        {{$level->translation->name}}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </form>
                                    </div>
                                </div>
                                <div class="skills-box-two">
                                    <div class="skill-form-two">
                                        <form action="" method="post">
                                            <span>Payment Type</span>
                                            @foreach($payments as $payment)
                                                <div class="radio-box">
                                                    <label class="p-0" for="payment-{{$loop->index}}">
                                                        <input type="checkbox" name="payment" id="payment-{{$loop->index}}">
                                                        {{$payment->translation->name}}
                                                    </label>
                                                </div>
                                            @endforeach
                                        </form>
                                    </div>
                                </div>
                                <div class="skills-box-three">
                                    <div class="skill-form-three">
                                        <form action="" method="post">
                                            <span>Duration Time</span>
                                            @foreach(range(1,5) as $duration)
                                                <div class="radio-box">
                                                    <label class="p-0" for="duration-{{$loop->index}}">
                                                        <input type="checkbox" name="duration" id="duration-{{$loop->index}}">
                                                        {{$duration}} Month
                                                    </label>
                                                </div>
                                            @endforeach
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="pagination-section">
                <ul>
                    <li class="prev">
                        <a href="#">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </li>
                    <li class="active">
                        <a href="" class="active">1</a>
                    </li>
                    <li>
                        <a href="">2</a>
                    </li>
                    <li>
                        <a href="">3</a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--Popular Courses-->

    <section class="most-courses">
        <div class="auto-container">
            <div class="sec-title">
                <h2>Most Popular Courses</h2>
            </div>
            <div class="row">
                <div class=" course-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <li class="inner-list">
                            <a href="course-detail.html" class="inner-element">
                                <img src="/assets/images/courses/course-3.jpg" alt="">
                            </a>
                            <div class="inside-content">
                                <h5><a href="course-detail.html">Visual Design</a></h5>
                                <div class="text mb-0">
                                    <p>
                                        Replenish of  third creature and meat blessed void a fruit gathered waters.
                                    </p>
                                </div>
                                <div class="inner-detail">
                                    <div class="inner-count">
                                        <span>125 Students</span>
                                    </div>
                                    <div class="inner-hour">
                                        <span>54 Hours</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </div>
                </div>
                <div class=" course-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <li class="inner-list">
                            <a href="course-detail.html" class="inner-element">
                                <img src="/assets/images/courses/course-4.jpg" alt="">
                            </a>
                            <div class="inside-content">
                                <h5><a href="course-detail.html">Visual Design</a></h5>
                                <div class="text mb-0">
                                    <p>
                                        Replenish of  third creature and meat blessed void a fruit gathered waters.
                                    </p>
                                </div>
                                <div class="inner-detail">
                                    <div class="inner-count">
                                        <span>125 Students</span>
                                    </div>
                                    <div class="inner-hour">
                                        <span>54 Hours</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </div>
                </div>
                <div class=" course-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <li class="inner-list">
                            <a href="course-detail.html" class="inner-element">
                                <img src="/assets/images/courses/course-5.jpg" alt="">
                            </a>
                            <div class="inside-content">
                                <h5><a href="course-detail.html">Visual Design</a></h5>
                                <div class="text mb-0">
                                    <p>
                                        Replenish of  third creature and meat blessed void a fruit gathered waters.
                                    </p>
                                </div>
                                <div class="inner-detail">
                                    <div class="inner-count">
                                        <span>125 Students</span>
                                    </div>
                                    <div class="inner-hour">
                                        <span>54 Hours</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="options-section" style="background-image: url('/assets/images/banners/3.png');">
        <div class="auto-container">
            <div class="content">
                <h2>Do you want to join us?</h2>
            </div>
            <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two <br> waters own morning gathered greater shall had behold had seed.</div>
            <div class="buttons-box">
                <a href="instuctor.html" class="theme-btn btn-start"><span class="txt">Become Instuctor <i class="fa fa-angle-right"></i></span></a>
                <a href="student.html" class="theme-btn btn-course"><span class="txt">Start Learning  <i class="fa fa-angle-right"></i></span></a>
            </div>
        </div>
    </section>

@endsection
