@extends('layouts.app')


@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/details.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')
    <section class="banner-section" style="background-image: url('/assets/images/banners/1.png');">
        <div class="auto-container">
            <div class="content-box">
                <div class="inner-column">
                    <h1>Learn Math, Science, English and Test <br> Prep from our Experts</h1>
                </div>
                <div class="buttons-box">
                    <a href="registration.html" class="btn-start">
                        <span class="txt">Get Stared <i class="fa fa-angle-right"></i></span>
                    </a>
                    <a href="{{route('courses')}}" class="btn-course">
                        <span class="txt">All Courses<i class="fa fa-angle-right"></i></span>
                    </a>
                </div>
            </div>
            <div class="image-layer">
                <img src="/assets/images/banners/banner.png" alt="">
            </div>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?"
                                   required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--Education Section-->
    <section class="education-section">
        <div class="patern-layer-one" style="background-image: url('/assets/images/icons/icon-1.png');"></div>
        <div class="patern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="auto-container">
            <div class="row">
                <div class="image-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <img src="/assets/images/icons/education.png" alt="">
                    </div>
                </div>
                <div class="text-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <h2>Our education system works for you</h2>
                        <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re,
                            they’re two waters own morning gathered greater shall had behold had seed.
                        </div>
                        <a href="course.html" class="theme-btn btn-course"><span class="txt">Learn More <i
                                    class="fa fa-angle-right"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Courses Section-->
    <section class="course-section">
        <div class="auto-container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 title-col">
                    <div class="inner-col">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <h2>Our top courses</h2>
                            <div class="text">Replenish him third creature and meat blessed void a fruit gathered
                                you’re, they’re two waters own morning gathered.
                            </div>
                        </div>
                        <a href="course.html" class="theme-btn btn-style-three"><span class="txt">Get Stared <i
                                    class="fa fa-angle-right"></i></span></a>
                    </div>
                </div>
                @foreach($courses as $course)
                    <div class="col-lg-4 col-md-4 col-sm-6 course-col">
                        <div class="inner-box">
                            <div class="image">
                                <a href="{{route('course',['slug' => $course->translation->slug])}}">
                                    <img src="{{\Illuminate\Support\Facades\Storage::url($course->image)}}" alt="">
                                </a>
                            </div>
                            <div class="inner-content">
                                <div class="title-part">
                                    <div class="left-side">
                                        <a href="{{route('course',['slug' => $course->translation->slug])}}"
                                           class="detail-link">
                                            {{$course->translation->name}}
                                        </a>
                                    </div>
                                    <div class="right-side">
                                        <span class="price">
                                            ${{(int)$course->price}}
                                        </span>
                                    </div>
                                </div>

                                <div class="text">{{\Illuminate\Support\Str::limit($course->translation->about)}}</div>

                                <div class="apply-box">
                                    <div class="count">
                                        125 Students
                                    </div>
                                    <div class="enroll">
                                        <a href="{{route('course',['slug' => $course->translation->slug])}}">
                                            Enroll now
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <!--Three Part Section-->
    <section class="type-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our Courses</h2>
                <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re
                    <br> two waters own morning gathered greater shall had behold had seed.
                </div>
            </div>
            <div class="course-type">
                <div class="row">
                    @foreach($courses as $course)
                        <div class="type-col col-lg-4 col-md-6 col-sm-12">
                            <div class="inner">
                                <div class="content">
                                    <div class="iconbox">
                                        <div class="icon">
                                            <img src="{{\Illuminate\Support\Facades\Storage::url($course->image)}}" alt="">
                                        </div>
                                    </div>
                                    <h4 class="course-title">{{$course->translation->name}}</h4>
                                    <a href="{{route('course',['slug' => $course->translation->slug])}}" class="btn-pink">
                                        Read more
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <!--News Section-->
    <section class="news-section">
        <div class="auto-container">
            <div class="row">
                <div class="news-col col-lg-6 col-md-6 col-sm-6">
                    <div class="inner-col">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <h2>Our latest news</h2>
                            <div class="text">Replenish him third creature and meat blessed void a fruit gathered
                                you’re, they’re two waters own morning gathered greater shall had behold had seed.
                            </div>
                        </div>
                        <a href="course.html" class="theme-btn btn-style-three"><span class="txt">Get Stared <i
                                    class="fa fa-angle-right"></i></span></a>
                    </div>
                </div>
                <div class="news-block col-lg-6 col-md-6 col-sm-6">
                    <div class="inner-box">
                        <div class="image">
                            <a href="news-detail">
                                <img src="/assets/images/news/news-1.jpg" alt="">
                            </a>
                        </div>
                        <div class="news-content">
                            <h3>
                                <a href="">To apply signal detection theory</a>
                            </h3>
                            <div class="text">Replenish him third creature and meat blessed void a fruit gathered
                                you’re, they’re two waters
                            </div>
                            <a href="blog-detail.html" class="read-more">Continue Reading</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Partners Section-->
    <section class="partner-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our Partners</h2>
                <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re
                    <br> two waters own morning gathered greater shall had behold had seed.
                </div>
            </div>

            <div class="row">
                <div class="company-col col-lg-6 col-md-6 col-sm-12">
                    <div class="owl-carousel owl-theme" id="company">
                        @foreach($companies as $company)
                            <div class="item">
                                <img src="{{\Illuminate\Support\Facades\Storage::url($company->image)}}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <a href="{{route('companies')}}" class="btn-partner">
                        <span class="txt">Companies <i class="fa fa-angle-right"></i></span>
                    </a>
                </div>
                <div class="university-col col-lg-6 col-md-6 col-sm-12">
                    <div class="owl-carousel owl-theme" id="university">
                        @foreach($universities as $university)
                            <div class="item">
                                <img src="{{\Illuminate\Support\Facades\Storage::url($university->image)}}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <a href="{{route('universities')}}" class="btn-partner">
                        <span class="txt">Universities <i class="fa fa-angle-right"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!--Option Section-->

    <section class="options-section" style="background-image: url('/assets/images/banners/3.png');">
        <div class="auto-container">
            <div class="content">
                <h2>Do you want to join us?</h2>
            </div>
            <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two
                <br> waters own morning gathered greater shall had behold had seed.
            </div>
            <div class="buttons-box">
                <a href="instuctor.html" class="theme-btn btn-start"><span class="txt">Become Instuctor <i
                            class="fa fa-angle-right"></i></span></a>
                <a href="student.html" class="theme-btn btn-course"><span class="txt">Start Learning  <i
                            class="fa fa-angle-right"></i></span></a>
            </div>
        </div>
    </section>


@endsection

