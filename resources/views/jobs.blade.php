@extends('layouts.app')

@section('css-app')
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')

    <section class="course-banner-section" >
        <div class="auto-container">
            <h1>Jobs</h1>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?" required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--Main Section-->

    <section class="job-section">
        <div class="patern-layer-one paroller" data-paroller-factor="0.40" data-paroller-factor-lg="0.20" data-paroller-type="foreground" data-paroller-direction="vertical" style="background-image: url('/assets/images/icons/icon-1.png'); transform: translateY(-51px); transition: transform 0s linear 0s; will-change: transform;"></div>
        <div class="patern-layer-two paroller" data-paroller-factor="0.40" data-paroller-factor-lg="-0.20" data-paroller-type="foreground" data-paroller-direction="vertical" style="background-image: url('/assets/images/icons/icon-2.png'); transform: translateY(13px); transition: transform 0s linear 0s; will-change: transform;"></div>
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Jobs List</h2>
                <div class="text">Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two <br> waters own morning gathered greater shall had behold had seed.</div>
            </div>
            <div class="row clearfix">

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-code"></span>
                        </div>
                        <h5><a href="course.html">Development</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <div class="enroll job-en">
                                    <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                        Apply
                                    </button>
                                    <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                        <div class="modal-dialog">

                                            <!-- Modal Content: begins -->
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                    <div>
                                                        <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                                    </div>
                                                    <div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                </div>

                                                <!-- Modal Body -->
                                                <div class="modal-body">
                                                    <div class="body-message">
                                                        <h4>Any Heading</h4>
                                                        <p>And a paragraph with a full sentence or something else...</p>
                                                    </div>
                                                </div>

                                                <!-- Modal Footer -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>

                                            </div>
                                            <!-- Modal Content: ends -->

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                    Apply
                                </button>
                                <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <!-- Modal Content: begins -->
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <div>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                                </div>
                                                <div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>

                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="body-message">
                                                    <h4>Any Heading</h4>
                                                    <p>And a paragraph with a full sentence or something else...</p>
                                                </div>
                                            </div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- Modal Content: ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                    Apply
                                </button>
                                <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <!-- Modal Content: begins -->
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <div>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                                </div>
                                                <div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>

                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="body-message">
                                                    <h4>Any Heading</h4>
                                                    <p>And a paragraph with a full sentence or something else...</p>
                                                </div>
                                            </div>
                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- Modal Content: ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                    Apply
                                </button>
                                <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <!-- Modal Content: begins -->
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <div>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                                </div>
                                                <div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>

                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="body-message">
                                                    <h4>Any Heading</h4>
                                                    <p>And a paragraph with a full sentence or something else...</p>
                                                </div>
                                            </div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                        <!-- Modal Content: ends -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                    Apply
                                </button>
                                <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                    <div class="modal-dialog">

                                        <!-- Modal Content: begins -->
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <div>
                                                    <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                                </div>
                                                <div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                </div>
                                            </div>

                                            <!-- Modal Body -->
                                            <div class="modal-body">
                                                <div class="body-message">
                                                    <h4>Any Heading</h4>
                                                    <p>And a paragraph with a full sentence or something else...</p>
                                                </div>
                                            </div>

                                            <!-- Modal Footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                        <!-- Modal Content: ends -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="apply-box">
                            <div class="enroll job-en">
                                <a href="course.detail.html">
                                    Apply
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="enroll job-en">
                            <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                Apply
                            </button>
                            <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                <div class="modal-dialog">

                                    <!-- Modal Content: begins -->
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <div>
                                                <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                            </div>
                                            <div>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="body-message">
                                                <h4>Any Heading</h4>
                                                <p>And a paragraph with a full sentence or something else...</p>
                                            </div>
                                        </div>

                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                    <!-- Modal Content: ends -->

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Topic Block -->
                <div class="topic-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="icon-box">
                            <a href="course.html" class="overlay-link"></a>
                            <span class="icon flaticon-graphic"></span>
                        </div>
                        <h5><a href="course.html">Business</a></h5>
                        <div class="text">Replenish him third creature and meat blessed</div>
                        <div class="enroll job-en">
                            <button type="button" class="apply-btn" data-toggle="modal" data-target=".bs-example-modal-new">
                                Apply
                            </button>
                            <div class="modal fade bs-example-modal-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                <div class="modal-dialog">

                                    <!-- Modal Content: begins -->
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <div>
                                                <h4 class="modal-title" id="gridSystemModalLabel">Your Headings</h4>
                                            </div>
                                            <div>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="body-message">
                                                <h4>Any Heading</h4>
                                                <p>And a paragraph with a full sentence or something else...</p>
                                            </div>
                                        </div>

                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                    <!-- Modal Content: ends -->

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="pagination-section">
                <ul>
                    <li class="prev">
                        <a href="#">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                    </li>
                    <li class="active">
                        <a href="" class="active">1</a>
                    </li>
                    <li>
                        <a href="">2</a>
                    </li>
                    <li>
                        <a href="">3</a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </section>

@endsection
