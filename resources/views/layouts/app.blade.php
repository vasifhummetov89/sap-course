<!DOCTYPE html>
<html lang="{{\Illuminate\Support\Facades\App::getLocale()}}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title','BITE Cource')</title>
    <!--CSS Files-->
    @yield('css-app')
    <link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="icon" href="" type="image/x-icon">
</head>
<body>
<div class="page-wrapper">
    <!--Preloader-->
    <div class="preloader" style="display: none;"></div>
    <!--Main Header-->
    <header class="main-header" @if(
                                    request()->routeIs('contact') ||
                                    request()->routeIs('student.settings') ||
                                    request()->routeIs('register') ||
                                    request()->routeIs('login')
                                    ) style="background-color:black;"
                                @endif >
        <div class="header-top">
            <div class="auto-container">
                <div class="top">
                    <div class="top-info">
                        <div class="number-info">
                            <a href="tel:+994(70)9807060" >
                                <img src="/assets/images/icons/telephone.png" alt="" >

                                +994(70)9807060
                            </a>
                        </div>
                        <div class="mail-info">
                            <a href="#" class="">
                                <img src="/assets/images/icons/email.svg" alt="">

                                info@yourcompany.com
                            </a>
                        </div>
                    </div>
                    <div class="top-right">
                        <ul class="login-nav">
                            @if(!auth()->guard('web')->check() && !auth()->guard('speaker')->check())
                                <li>
                                    <a href="{{route('login')}}">
                                        Log In
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('register')}}">
                                        @lang('content.course')
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="navigation-wrap start-header start-style">
            <div class="auto-container">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-md navbar-light">

                            <a class="navbar-brand" href="{{route('index')}}"><img src="/assets/images/icons/logo.png" alt=""></a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto py-4 py-md-0 navigation">

                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4 active">
                                        <a class="nav-link" href="{{route('index')}}" >Home</a>
                                    </li>

                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">About Us</a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('about')}}">About us</a>
                                            <a class="dropdown-item" href="{{route('courses')}}">Our courses</a>
                                            <a class="dropdown-item" href="course.html">News</a>
                                            <a class="dropdown-item" href="course.html">Events</a>
                                            <a class="dropdown-item" href="{{route('jobs')}}">Jobs</a>
                                        </div>
                                    </li>
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Courses</a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="course-detail.html">Business Management</a>
                                            <a class="dropdown-item" href="course-detail.html">Information Technology</a>
                                            <a class="dropdown-item" href="course-detail.html">SAP Solutions</a>
                                        </div>
                                    </li>
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Partnership</a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('universities')}}">Universities</a>
                                            <a class="dropdown-item" href="{{route('companies')}}">Companies</a>
                                        </div>
                                    </li>
                                    <li class="nav-item pl-4 pl-md-0 ml-0 ml-md-4">
                                        <a class="nav-link" href="{{route('contact')}}">Contact</a>
                                    </li>
                                    @auth('web')
                                        <li class="h-lang">
                                            <a class="h-lang-link dropdownButton">
                                                {{auth()->user()->full_name}}
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </a>

                                            <ul class="h-lang-list dropdownMenu" style="display: none;">
                                                <li>
                                                    <a href="{{route('student.settings')}}">
                                                        Settings
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('logout')}}">
                                                        Logout
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    @endauth

                                    @auth('speaker')
                                        <li class="h-lang">
                                            <a class="h-lang-link dropdownButton">
                                                {{auth()->guard('speaker')->user()->full_name}}
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </a>

                                            <ul class="h-lang-list dropdownMenu" style="display: none;">
                                                <li>
                                                    <a href="{{route('teacher.settings')}}">
                                                        Settings
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{route('logout')}}">
                                                        Logout
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                    @endauth

                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="main-footer">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="auto-container">
            <div class="footer-section">
                <div class="row">
                    <div class="big-col col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="footer-col col-lg-3 col-md-3 col-sm-12">
                                <div class="logo-widget">
                                    <div class="logo">
                                        <a href="index.html">
                                            <img src="/assets/images/icons/footer-logo.png" alt="">
                                        </a>
                                    </div>
                                    <div class="text">
                                        Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two waters own morning gathered greater.
                                    </div>
                                    <div class="social-icons">
                                        <a href="#">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fab fa-linkedin-in"></i>
                                        </a>
                                    </div>
                                    <div class="copyright">
                                        Copyright © 2021
                                    </div>
                                </div>
                            </div>
                            <div class="footer-col col-lg-3 col-md-3 col-sm-12">
                                <div class="links-widget">
                                    <h4>
                                        About Us
                                    </h4>
                                    <ul class="links-content">
                                        <li><a href="#">Afficiates</a></li>
                                        <li><a href="#">Partners</a></li>
                                        <li><a href="#">Reviews</a></li>
                                        <li><a href="#">Blogs</a></li>
                                        <li><a href="#">Newsletter</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="footer-col col-lg-3 col-md-3 col-sm-12">
                                <div class="links-widget">
                                    <h4>
                                        Resource
                                    </h4>
                                    <ul class="links-content">
                                        <li><a href="#">Afficiates</a></li>
                                        <li><a href="#">Partners</a></li>
                                        <li><a href="#">Reviews</a></li>
                                        <li><a href="#">Blogs</a></li>
                                        <li><a href="#">Newsletter</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="footer-col col-lg-3 col-md-3 col-sm-12">
                                <div class="links-widget">
                                    <h4>
                                        quick Links
                                    </h4>
                                    <ul class="links-content">
                                        <li><a href="#">Afficiates</a></li>
                                        <li><a href="#">Partners</a></li>
                                        <li><a href="#">Reviews</a></li>
                                        <li><a href="#">Blogs</a></li>
                                        <li><a href="#">Newsletter</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>


<script src="/assets/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/owl.carousel.min.js"></script>
<script src="/assets/js/main.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(session()->has('success'))

    <script>
        Swal.fire(
            '{{session()->get('title')}}',
            '{{session()->get('message')}}',
            '{{session()->get('icon')}}'
        )
    </script>

@endif

@yield('js')
</body>
</html>
