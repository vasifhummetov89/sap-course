@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection


@section('content')

    <!--Login Section-->
    <section class="register-section">
        <div class="register-content">
            <div class="auto-container">
                <div class="row">
                    <div class="register-body">
                        <div class="col-12">
                            <form id="form-sign-up" action="{{route('sign-in')}}" method="POST" autocomplete="on">
                                <div class="title-box col-12">
                                    <h2>Daxil Ol</h2>
                                    <div class="text">Şəxsi kabinetə keçid</div>
                                </div>
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" name="email" value="{{old('email')}}" placeholder="Email" id="email" >
                                        </div>

                                        @error('email')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <i class="fas fa-eye eye-icon showing-password"></i>
                                            <input type="password" id="password" name="password" value="" placeholder="Password" >
                                        </div>
                                        @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group password-sec">
                                            <div class="pull-left">
                                                <div class="check-box">
                                                    <input type="checkbox" name="remember" id="type-1">
                                                    <label for="type-1">Remember me</label>
                                                </div>
                                            </div>
                                            <div class="pull-right">
                                                <a href="#" class="forgot">Forget Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group text-center">
                                            <button type="submit" class="theme-btn btn-style-three"><span class="txt">Log In</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')

    <script>
        $(document).ready(function (){
            $(document).on('click', '.showing-password', function () {
                $('#password').attr('type', 'text');
                $(this).addClass('showing')
            })
            $(document).on('click','.showing',function (){
                $('#password').attr('type','password');
                $(this).removeClass('showing')
            })
        });
    </script>

@endsection
