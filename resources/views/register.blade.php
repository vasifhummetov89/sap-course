@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="/assets/build/css/intlTelInput.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')
    <section class="register-section">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="register-content">
            <div class="auto-container">
                <div class="row">
                    <div class="register-body">
                        <div class="col-12">
                            <form id="form-sign-up" action="{{route('register.store')}}" method="POST" autocomplete="off">
                                <div class="title-box col-12">
                                    <h2>Qeydiyyatdan keç</h2>
                                    <div class="text">Şəxsi hesabınızı yaradın</div>
                                </div>
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" value="{{old('name')}}" placeholder="Name" id="name" required="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="surname"> Surname</label>
                                            <input type="text" name="surname" value="{{old('surname')}}" placeholder="Surname" id="surname" required="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" id="email" name="email" value="{{old('email')}}" placeholder="Email" required="">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="tel">Telephone Number</label>
                                            <input type="number" class="form-control" id="tel" name="phone" value="{{old('phone')}}" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <i class="fas fa-eye eye-icon showing-password"></i>
                                            <input type="password" id="password" name="password" value="" placeholder="Password" required="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <i class="fas fa-eye eye-icon password-confirmation"></i>
                                            <input type="password" id="password_confirmation" name="password_confirmation" value="" placeholder="Password confirm" required="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group text-center">
                                            <button type="submit" class="theme-btn btn-style-three"><span class="txt">Register</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        $(document).ready(function (){

            $(document).on('click', '.showing-password', function () {
                $('#password').attr('type', 'text');
                $(this).addClass('showing')
            })

            $(document).on('click','.showing',function (){
                $('#password').attr('type','password');
                $(this).removeClass('showing')
            })


            $(document).on('click', '.password-confirmation', function () {
                $('#password_confirmation').attr('type', 'text');
                $(this).addClass('showing-p')
            })

            $(document).on('click','.showing-p',function (){
                $('#password_confirmation').attr('type','password');
                $(this).removeClass('showing-p')
            })

        });
    </script>

    <script src="/assets/build/js/intlTelInput-jquery.min.js"></script>
    <script>
        $("#tel").intlTelInput({// whether or not to allow the dropdown
            allowDropdown: true,

// if there is just a dial code in the input: remove it on blur, and re-add it on focus
            autoHideDialCode: true,

// add a placeholder in the input with an example number for the selected country
            autoPlaceholder: "polite",

// modify the auto placeholder
            customPlaceholder: null,

// append menu to specified element
            dropdownContainer: null,

// don't display these countries
            excludeCountries: [],

// format the input value during initialisation and on setNumber
            formatOnDisplay: true,

// geoIp lookup function
            geoIpLookup: null,

// inject a hidden input with this name, and on submit, populate it with the result of getNumber
            hiddenInput: "",

// initial country
            initialCountry: "",

// localized country names e.g. { 'de': 'Deutschland' }
            localizedCountries: null,

// don't insert international dial codes
            nationalMode: true,

// display only these countries
            onlyCountries: [],

// number type to use for placeholders
            placeholderNumberType: "MOBILE",

// the countries at the top of the list. defaults to united states and united kingdom
            preferredCountries: [ "us", "gb" ],

// display the country dial code next to the selected flag so it's not part of the typed number
            separateDialCode: false,

// specify the path to the libphonenumber script to enable validation/formatting
            utilsScript: ""
        });

    </script>

@endsection


