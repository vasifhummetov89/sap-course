@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="/assets/build/css/intlTelInput.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.css" rel="stylesheet">
    <link href="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')

    <section class="student-profile-section">
        <div class="pattern-layer" style="background-image:url('/assets/images/icons/icon-1.png')"></div>
        <div class="pattern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="edit-content">
            <div class="auto-container">
                <div class="row">
                    <div class="image-column col-lg-3 col-md-12 col-sm-12">
                        <div class="inner-column">
                            <div class="image">
                                <img id="output" src="/assets/images/courses/student-2.jpg" alt="">
                            </div>
                            <div class="about-student">
                                <h4>{{auth()->user()->full_name}}</h4>
                                <div class="text">{{auth()->user()->course()->first()->translation->name}}</div>
                                <div class="group">B Group</div>
                                <ul class="student-editing">
                                    <li><a href="edit-profile.html">Edit Account</a></li>
                                    <li><a href="">Delete Profile</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12">
                        <div class='cl-toggle'>
                            <div class='cl-tabs'>
                                <div class='cl-tab active'>Courses</div>
                                <div class='cl-tab'>Calendar</div>
                                <div class='cl-tab'>Courses in Progress</div>
                                <div class='cl-tab'>Certification</div>
                                <div class="cl-tab">Exam Scores</div>
                                <div class="cl-tab">Exam Links</div>
                            </div>
                            <div class='cl-panels'>
                                <div class='cl-panel'>
                                    <div class="our-courses">
                                        <div class="inner-part">
                                            <ol class="list grid-view-filter">
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-3.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-4.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-5.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-6.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-7.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-8.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Visual Design</a></h5>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class='cl-panel'>
                                    <div id='calendar'></div>
                                </div>
                                <div class='cl-panel panel-certificate'>
                                    <div class="our-courses">
                                        <div class="inner-part">
                                            <ol class="list grid-view-filter">
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-3.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">SAP Course</a></h5>
                                                    </div>
                                                </li>
                                                <li class="inner-list">
                                                    <a href="course-detail.html" class="inner-element">
                                                        <img src="/assets/images/courses/course-4.jpg" alt="">
                                                    </a>
                                                    <div class="inside-content">
                                                        <h5><a href="course-detail.html">Business Management</a></h5>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class='cl-panel'>
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="certificate">
                                                <a href="/assets/images/certificate.pdf" class="certificate-content" target="_blank">Certificate SAP</a>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="certificate">
                                                <a href="/assets/images/certificate.pdf" class="certificate-content" target="_blank">Business Management</a>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="certificate">
                                                <a href="/assets/images/certificate.pdf" class="certificate-content" target="_blank">Mobile Application</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cl-panel">
                                    <div id="wrapper">
                                        <h1>Course Exam's Scores</h1>

                                        <table id="keywords" cellspacing="0" cellpadding="0">
                                            <thead>
                                            <tr>
                                                <th><span>Exams</span></th>
                                                <th><span>1st Week</span></th>
                                                <th><span>2nd Week</span></th>
                                                <th><span>3rd Week</span></th>
                                                <th><span>4th Week</span></th>
                                                <th><span>Average Score %</span></th>
                                                <th><span>Status</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="lalign">SAP SUHAN</td>
                                                <td>30%</td>
                                                <td>40%</td>
                                                <td>50%</td>
                                                <td>70%</td>
                                                <td>47,5 %</td>
                                                <td>Passed</td>
                                            </tr>
                                            <tr>
                                                <td class="lalign">PM Entry</td>
                                                <td>30%</td>
                                                <td>40%</td>
                                                <td>50%</td>
                                                <td>70%</td>
                                                <td>47,5 %</td>
                                                <td>Passed</td>
                                            </tr>
                                            <tr>
                                                <td class="lalign">PM Entry</td>
                                                <td>30%</td>
                                                <td>40%</td>
                                                <td>50%</td>
                                                <td>70%</td>
                                                <td>47,5 %</td>
                                                <td>Passed</td>
                                            </tr>
                                            <tr>
                                                <td class="lalign">PM Entry</td>
                                                <td>30%</td>
                                                <td>40%</td>
                                                <td>50%</td>
                                                <td>70%</td>
                                                <td>47,5 %</td>
                                                <td>Passed</td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="cl-panel">
                                    <div class="container">
                                        <div class="exam-links">Your Exam links are there!</div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="link-content col-lg-5 col-md-5 col-6">
                                                        <div class="link-title">
                                                            SAP 1st Lesson
                                                        </div>
                                                        <span>
                                                            For attending at the exam please click link!
                                                        </span>
                                                        <div class="link-inner">
                                                            <a href= https://www.shecodes.io/free-class#learn">SAP 1st Exam </a>
                                                        </div>
                                                    </div>
                                                    <div class="link-content col-lg-5 col-md-5 col-6">
                                                        <div class="link-title">
                                                            SAP 1st Lesson
                                                        </div>
                                                        <span>
                                                            For attending at the exam please click link!
                                                        </span>
                                                        <div class="link-inner">
                                                            <a href= https://www.shecodes.io/free-class#learn">SAP 1st Exam </a>
                                                        </div>
                                                    </div>
                                                    <div class="link-content col-lg-5 col-md-5 col-6">
                                                        <div class="link-title">
                                                            SAP 1st Lesson
                                                        </div>
                                                        <span>
                                                            For attending at the exam please click link!
                                                        </span>
                                                        <div class="link-inner">
                                                            <a href= https://www.shecodes.io/free-class#learn">SAP 1st Exam </a>
                                                        </div>
                                                    </div>
                                                    <div class="link-content col-lg-5 col-md-5 col-6">
                                                        <div class="link-title">
                                                            SAP 1st Lesson
                                                        </div>
                                                        <span>
                                                            For attending at the exam please click link!
                                                        </span>
                                                        <div class="link-inner">
                                                            <a href= https://www.shecodes.io/free-class#learn">SAP 1st Exam </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')

    <script src="https://cdn.jsdelivr.net/npm/rrule@2.6.3/dist/es5/rrule.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/core@4.3.1/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/daygrid@4.3.0/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/interaction@4.3.0/main.min.js"></script>
    <script src="https://unpkg.com/@fullcalendar/rrule@4.3.0/main.min.js"></script>
    <script src="/assets/js/tab.js"></script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {


                plugins: [ 'dayGrid','rrule'],
                timeZone: 'UTC',
                defaultView: 'dayGridMonth',
                events: [
                    {
                        title: 'TEST #1 my recurring event',
                        rrule:'DTSTART:20191111T103000Z\nRRULE:FREQ=WEEKLY;INTERVAL=1;UNTIL=20290601;BYDAY=TU,TH',
                        duration: '2:00'
                    },
                    {
                        title: 'TEST #2 my recurring event',
                        backgroundColor: 'lime',
                        rrule: {
                            freq: 'weekly',
                            interval: 2,
                            byweekday: [ 'mo', 'fr' ],
                            dtstart: '2019-11-11T10:30:00',
                            until: '2029-06-01'
                        },
                        duration: '01:00'
                    }
                ]

            });


            calendar.render();
        });
    </script>

@endsection
