@extends('layouts.app')

@section('css-app')

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/reset.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/details.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">

@endsection

@section('content')

    <section class="course-banner-section" >
        <div class="auto-container">
            <h1> University Partners</h1>
            <div class="search-box-container">
                <div class="search-box">
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="search" name="search-field" value placeholder="What do you want to learn?" required>
                            <button type="submit">
                                <span class="icon fa fa-search"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="education-section section-second">
        <div class="patern-layer-one" style="background-image: url('/assets/images/icons/icon-1.png');"></div>
        <div class="patern-layer-two" style="background-image: url('/assets/images/icons/icon-2.png');"></div>
        <div class="auto-container">
            <div class="row">
                <div class="image-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <img src="/assets/images/courses/c.png" alt="">
                    </div>
                </div>
                <div class="text-col col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-col">
                        <h3>What we are doing with companies?</h3>
                        <div class="text">
                            <p>
                                Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two waters own morning gathered greater shall had behold had seed.
                                Replenish him third creature and meat blessed void a fruit gathered you’re, they’re two waters own morning gathered greater shall had behold had seed.

                            </p>
                        </div>
                        <div class="info p-20 bg-black-333">
                            <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Email</strong><br> smith@yourdomain.com</li>
                            <li class="text-gray-silver"><strong class="text-gray-lighter">Web</strong><br> www.yourdomain.com</li>
                            <div class="social-icons">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="teachers-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2>University Partners</h2>
            </div>

            <!-- Fact Counter -->
            <div class="fact-counter">
                <div class="row ">
                    @foreach($universities as $university)
                        <div class="col-lg-3 col-md-3 col-sm-6 course-col">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="javascript:void(0)">
                                        <img src="{{\Illuminate\Support\Facades\Storage::url($university->image)}}"  class= "inner-image-uni" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


@endsection
