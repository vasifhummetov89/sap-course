<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::prefix(\App\Http\Localization\Localization::getCurrentLocale())->group(function(){

    Route::get('/', [\App\Http\Controllers\HomeController::class,'index'])->name('index');

    Route::get('universities',[\App\Http\Controllers\UniversityController::class,'index'])->name('universities');

    Route::get('companies',[\App\Http\Controllers\HomeController::class,'companies'])->name('companies');

    Route::get('courses',[\App\Http\Controllers\HomeController::class,'courses'])->name('courses');

    Route::get('course/{slug?}',[\App\Http\Controllers\HomeController::class,'course'])->name('course');

    Route::get('about',[\App\Http\Controllers\HomeController::class,'about'])->name('about');

    Route::get('jobs',[\App\Http\Controllers\HomeController::class,'jobs'])->name('jobs');

    Route::get('contact',[\App\Http\Controllers\HomeController::class,'contact'])->name('contact');

    Route::get('register',[\App\Http\Controllers\HomeController::class,'register'])->name('register');

    Route::get('login',[\App\Http\Controllers\HomeController::class,'login'])->name('login');

    Route::post('login',[\App\Http\Controllers\HomeController::class,'signIn'])->name('sign-in');

    Route::post('register',[\App\Http\Controllers\HomeController::class,'registerStore'])->name('register.store');

    Route::get('logout',[\App\Http\Controllers\HomeController::class,'logout'])->name('logout');

    Route::get('/teacher/settings',[\App\Http\Controllers\HomeController::class,'teacher_settings'])->name('teacher.settings');

    Route::get('/student/settings',[\App\Http\Controllers\HomeController::class,'student_settings'])->name('student.settings');

});




Route::name('admin.')->prefix('admin')->group(function(){

    Route::get('authentication/login',[\App\Http\Controllers\Admin\LoginController::class,'login']);
    Route::post('authentication/login',[\App\Http\Controllers\Admin\LoginController::class,'auth'])->name('login');

    Route::middleware(['admin'])->group(function (){

        Route::get('dashboard',[\App\Http\Controllers\Admin\LoginController::class,'dashboard'])->name('dashboard');
        Route::get('logout',[\App\Http\Controllers\Admin\LoginController::class,'logout'])->name('logout');

        Route::resources([

            'course'        => \App\Http\Controllers\Admin\CourseController::class,
            'partner'       => \App\Http\Controllers\Admin\PartnerController::class,
            'university'    => \App\Http\Controllers\Admin\UniversityController::class,
            'company'       => \App\Http\Controllers\Admin\CompanyController::class,
            'level'         => \App\Http\Controllers\Admin\LevelController::class,
            'payment-type'  => \App\Http\Controllers\Admin\PaymentTypeController::class,
            'language'      => \App\Http\Controllers\Admin\LanguageController::class,
            'speaker'       => \App\Http\Controllers\Admin\SpeakerController::class,



        ]);


    });

});
